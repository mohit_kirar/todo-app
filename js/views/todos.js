var app = app || {};

// Todo item view

// DOM element for a todo item ...
app.TodoView = Backbone.View.extend({

	// ... is a list tag
	tagName : 'li',

	// Cache the template function for a single item
	template : _.template( $('#item-template').html() ),

	// DOM events specific to an item
	events : {
		'click .toggle': 'togglecompleted',
		'dblclick label': 'edit',
		'click .destroy': 'clear',
		'keypress .edit': 'updateOnEnter',
		'blur .edit': 'close'
	},

	// Setting a direct reference on the model for convenience
	// TodoView will listen for changes to its model
	initialize : function() {
		this.listenTo(this.model, 'change', this.render);
		this.listenTo(this.model, 'destroy', this.remove);
		this.listenTo(this.model, 'visible', this.toggleVisible);
	},

	// for rendering the titles of the todo item.
	render : function() {
		this.$el.html( this.template( this.model.toJSON() ) );

		this.$el.toggleClass('completed',this.model.get('completed'));
		this.toggleVisible();

		this.$input = this.$('.edit');
		return this;
	},

	// Toggle the visibility of item
	toggleVisible : function() {
		this.$el.toggleClass('hidden', this.isHidden());
	},

	// Determines if the item should be hidden
	isHidden : function() {
		var isCompleted = this.model.get('completed');
		return (
			(!isCompleted && app.TodoFilter === 'completed')
			|| (isCompleted && app.TodoFilter === 'active')
			);
	},

	// Toggle the completed state of the model
	togglecompleted : function() {
		this.model.toggle();
	},

	// Switch this view into editing mode, displaying the editing field
	edit : function() {
		this.$el.addClass('editing');
		this.$input.focus();
	},

	// Closing the editing mode and saving the changes to Todo
	close : function() {
		var value = this.$input.val().trim();

		if(value){
			this.model.save({title : value});
		}

		this.$el.removeClass('editing');
	},

	// If you hit enter, editing the item is over
	updateOnEnter : function(e) {
		if(e.which === ENTER_KEY) {
			this.close();
		}
	},

	clear : function() {
		this.model.destroy();
	}
});
