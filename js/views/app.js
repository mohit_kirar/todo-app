var app = app || {};

// Overall AppView is the top-level piece of UI
app.AppView = Backbone.View.extend({

	//bind to the existing skeleton of the app already present in the HTML
	el : "#todoapp",

	//template for line of statistics
	statsTemplate : _.template( $('#stats-template').html() ),

	// Event handling
	events : {
		'keypress #new-todo': 'createOnEnter',
		'click #clear-completed': 'clearCompleted',
		'click #toggle-all': 'toogleAllComplete'
	},

	// It will fetch the previously saved todos from Local storage
	// On initialization we bind to the relevant events on the todos
	// collection, when items are added or changed
	initialize : function() {
		this.allCheckbox = this.$('#toggle-all')[0];
		this.$input = this.$('#new-todo');
		this.$footer = this.$('#footer');
		this.$main = this.$('#main');

		this.listenTo(app.Todos, 'add', this.addOne);
		this.listenTo(app.Todos, 'reset', this.addAll);
		this.listenTo(app.Todos, 'change:completed', this.filterOne);
		this.listenTo(app.Todos, 'filter', this.filterAll);
		this.listenTo(app.Todos, 'all', this.render);

		app.Todos.fetch();
	},

	render : function() {
		var completed = app.Todos.completed().length;
		var remaining = app.Todos.remaining().length;

		// show main and footer if there are Todos existing
		if( app.Todos.length ) {
			this.$main.show();
			this.$footer.show();

			// printing the no of todos
			this.$footer.html(this.statsTemplate({
				completed : completed,
				remaining : remaining
			}));

			this.$('#filters li a')
			.removeClass('selected')
			.filter('[href="#/' + ( app.TodoFilter || '' ) + '"]' )
			.addClass('selected');
		} else {
			this.$main.hide();
			this.$footer.hide();
		}

		this.allCheckbox.checked = !remaining;
	},

	// Add a single todo item to the list by creating a view for it
	// And appending its element to the <ul> 
	addOne : function(todo) {
		var view = new app.TodoView({ model : todo });
		$('#todo-list').append( view.render().el );
	},

	// Add all items in the Todos collection at once
	addAll : function() {
		this.$('#todo-list').html('');
		app.Todos.each( this.addOne, this );
	},

	filterOne : function(todo) {
		todo.trigger('visible');
	},

	filterAll : function() {
		app.Todos.each(this.filterOne, this);
	},

	// Generate the attributes for a new todo item
	newAttributes : function() {
		return {
			title : this.$input.val().trim(),
			order : app.Todos.nextOrder(),
			completed : false
		};
	},

	// On hitting Enter key create new Todo model and
	// persist it to localStorage
	createOnEnter : function( event ){
		if( event.which !== ENTER_KEY || !this.$input.val().trim() ){
			return;
		}

		app.Todos.create(this.newAttributes());
		// resets the input field
		this.$input.val('');
	},

	// destroy the models of all completed todos
	clearCompleted : function() {
		_.invoke(app.Todos.completed(), 'destroy');
		return false;
	},

	toggleAllComplete : function() {
		var completed = this.allCheckbox.checked;

		app.Todos.each(function( todo ){
			todo.save({
				'completed' : completed
			});
		});
	}
});