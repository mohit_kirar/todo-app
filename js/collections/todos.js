// The collection uses the local storage adapter to override Backbone's default sync() operation
// with one that will persist our todo records to HTML5 localstorage, to save them between
// page requests.

// for namespace

var app = app || {};

// The collection of todos is backed by
// "Local Storage" instead of a remote
// server

var TodoList = Backbone.Collection.extend({

	//reference to this collection's model
	model : app.Todo,

	// save all the todo items under todos-backbone namespace
	// in order for this to work, Backbone localstorage plug-in
	// must be loaded inside your page in order for this to work.
	// Comment it out if testing in console without that...
	localStorage : new Backbone.LocalStorage('todos-backbone'),

	//filter down all the completed todos
	completed : function() {
		return this.filter(function(todo){
			return todo.get('completed');
		});
	},

	//filter down the list of unfinished todos
	remaining : function() {
		//apply allow us to define the context of this within our function scope
		return this.without.apply(this,this.completed());
	}

	//to keep the todos in sequential order
	nextOrder : function() {
		if( !this.length ) {
			return 1;
		}
		return this.last().get('order')+1;
	},

	//sorted by their original insertion order
	comparator : function( todo ) {
		return todo.get('order');
	}

});

//the global collection of todos
app.Todos = new TodoList();

// this.filter, this.without and this.last are underscore methods mixed into Backbone.Collection