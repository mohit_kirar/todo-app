// for namespace 
var app = app || {};

// The basic todo contains 
// Title, Order and Completed attributes

app.Todo = Backbone.Model.extend({

	// for setting up default values
	defaults : {
		title : '',
		completed : false
	},

	// This will toggle the completed state
	// of this item
	// the change is set and simultaneously persisted
	toggle : function(){
		this.save({
			completed : !this.get('completed')
		});
	}

});